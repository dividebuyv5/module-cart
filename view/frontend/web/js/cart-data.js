define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'uiRegistry'
], function myscript($, ko, Component, customerData, registry, total) {
    'use strict';

    return Component.extend({

        /** @inheritdoc */
        initialize: function () {
            this._super();

            var self = this;
            var count = '';
            var total = '';

            customerData.get('cart').subscribe(function (cartData) {
                count = cartData.summary_count;
            }, this);
            customerData.get('cart-data').subscribe(function (cartData) {
                total = cartData.totals;
            }, this);
            setTimeout(function() {
                count = customerData.get('cart')().summary_count;
                total = customerData.get('cart-data')().totals;
                if(count == null || total == null){
                    if(localStorage.getItem("shipping_amount")){
                        localStorage.removeItem("shipping_amount");
                    }
                }
            }, 2000);
        },
    });
});


